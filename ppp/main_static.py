# -*- coding: utf-8 -*-

'''
Created on Mar 7, 2012

@author: Pavel Kostelnik
'''
from ppp.excelReader import ExcelReader
import sys,os
import time
from ppp.csfdInvestigator import CSFDInvestigator
from ppp.excelWriter import ExcelWriter
from fdbInvestigator import FDBInvestigator
class Main(object):
    '''
    This is the main app class in STATIC version
    '''
    def setFilePath(self):
        self.path = ".."+os.sep+"data"+os.sep+"tickets2short.xls"
        
        
    def consolidateDB(self, db):
        """
        This has to be done synchronized
        """
        for movie in self.movieDB.db:
            movie.setRating(db.findMovieByName(movie.name).rating)
                
    
    def main(self):
        self.start = time.time()
        self.setFilePath()
        reader = ExcelReader(self.path, self)
        reader.readSource()
        csfd = CSFDInvestigator(app=self)
        csfd.run()
        self.consolidateDB(csfd.movieDB)
        writer= ExcelWriter()
        writer.writeMovieDB(self.movieDB.db)
        fdb = FDBInvestigator(app=self)
        fdb.run()
        
        self.end = time.time()
        self.executionTime()
        
    def executionTime(self):
        print "This script in serial execution took "+str(self.end-self.start)+" seconds to execute"
        
    def __init__(self):
        '''
        Constructor
        '''
        pass
    
if __name__ == "__main__":
    app = Main()
    app.main()
        