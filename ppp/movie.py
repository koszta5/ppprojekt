'''
Created on Mar 7, 2012

@author: Pavel Kostelnik
'''

class Movie(object):
    '''
    Movie class - rating is a number ranging from 0 to 100
    '''
    def setRating(self, rating=0):
        if self.numberOfRatings:
            self.rating = (self.rating+rating)/(self.numberOfRatings+1)
            self.numberOfRatings += 1
        else:
            self.rating = rating
        
        

    def __init__(self, name="None"):
        '''
        Constructor
        '''
        self.name = name
        self.numberOfRatings = 0
        self.rating=0