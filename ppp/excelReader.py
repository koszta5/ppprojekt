'''
Created on Mar 7, 2012

@author: Pavel Kostelnik
'''
import xlrd

from movieDB import MovieDB
from movie import Movie
class ExcelReader(object):
    '''
    classdocs
    '''
    
    def readSource(self):
        """
        Creating a central DB of movies in app object - when using threads each thread will contain one movieDB instance
        """
        for rownum in range(3,self.excelSheet.nrows):
            movieName = self.excelSheet.row_values(rownum)[1]
            
            self.app.movieDB.addMovie(Movie(name=str(movieName)))
        

    def __init__(self, pathToFile, app):
        '''
        Constructor
        '''
        self.excelSheet = xlrd.open_workbook(pathToFile).sheet_by_index(0)
        self.app = app
        self.app.movieDB = MovieDB()
        