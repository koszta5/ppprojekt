# -*- coding: utf-8 -*-
'''
Created on Mar 7, 2012

@author: Pavel Kostelnik
'''
from ppp.investigator import Investigator
import re,os
import difflib
import unicodedata
from HTMLParser import HTMLParser

class CSFDInvestigator(Investigator):
    '''
    This class uses the CSFD as its main DB 
    '''
    
    class MyHTMLParser(HTMLParser):
        """
        Html parser to find movie in search results
        """
        recordData = False
        def handle_starttag(self, tag, attrs):
            if tag == "div":
                if attrs:
                    if attrs[0][0] == "id":
                        if attrs[0][1] == "search-films":
                            self.recordData = True
            if tag == "a":
                if len(attrs) > 1:
                        if attrs[1][0]== "class":
                            if re.match(r'^film c\d+.*',attrs[1][1]):
                                self.links.append(attrs[0][1])
                            
                            
                
        def handle_endtag(self, tag):
            if self.recordData == True:
                if tag == "div":
                    self.recordData = False
                
        def handle_data(self, data):
            if self.recordData:
                if re.match(r"^[a-záčďéěíňóřšťůúýž]+.*", data, re.IGNORECASE):
                    self.data.append(data)
        
        
    class MyRatingParser(HTMLParser):
        """
        Parser class to find rating in movie page
        """
        recordData = False
        def handle_starttag(self, tag, attrs):
            if tag == "h2":
                if attrs:
                    if attrs[0][0] == "class":
                        if attrs[0][1] == "average":
                            self.recordData = True
                
        def handle_endtag(self, tag):
            if self.recordData == True:
                if tag == "h2":
                    self.recordData = False
                
        def handle_data(self, data):
            if self.recordData:
                if re.match(r"\d+.*", data):
                    data = re.sub("%", "", data)
                    self.rating.append(data)
    
    class MyTranslationParser(HTMLParser):
        """
        Parser class to find movie translation
        """
        def __init__(self):
            HTMLParser.__init__(self)
            self.translation = ""
        recordData = False
        def handle_starttag(self, tag, attrs):
            if tag == "img":
                if len(attrs) > 1:
                    if attrs[1][1] == "USA":
                        self.recordData = True
                        
        def handle_endtag(self, tag):
            if self.recordData:
                if tag=="h3":
                    self.recordData = False
                
        def handle_data(self,data):
            if self.recordData:
                if re.match(r"^[a-záčďéěíňóřšťůúýž]+.*", data, re.IGNORECASE):
                    data = self.unescape(data)
                    self.translation += data
        
                    
        
    def __init__(self, app,  baseURL="http://www.csfd.cz"):
        '''
        Constructor
        '''
        super(CSFDInvestigator, self).__init__(baseURL, app)
        self.parser = self.MyHTMLParser()
        self.ratingParser = self.MyRatingParser()
        self.translationParser = self.MyTranslationParser()
        

    def prepareQuery(self, movieName, search=True):
        if search:
            url = self.baseURL
            url += "/hledat/?q="
            movieQuery = re.sub(" ", "+", movieName)
            url += movieQuery
            return url
        else:
            url = self.baseURL+movieName
            return url
        

    def getTranlatedMovieTitle(self, title):
        def strip_accents(s):
            return ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))
        """
        Convinience function for other Investigators (translates czech name to english)
        @param title: Name of the movie in CZ
        @return: Name of the movie in ENG if found, if not name of the movie with accents stripped
        """
        tempFile = self.download(title)
        translateFile = self.parse(tempFile, title)
        translatedTitle = self.parseTranslation(translateFile)
        if translatedTitle:
            return translatedTitle
        else:
            title = unicode(title)
            return strip_accents(title)
            
        
        

    
    def parse(self, tempFile, movieName):
        """
        Parse HTML and download the particular page
        """
        self.parser.data = []
        self.parser.links = []
        tempFileStream = open(tempFile.name, "r")
        lines = tempFileStream.readlines()
        tempFileStream.close()
        for line in lines:
            self.parser.feed(line)
        os.remove(tempFile.name)
        if self.parser.data:
            uni = self.parser.data[1] 
            if difflib.SequenceMatcher(None, uni, movieName).ratio() > 0.8:
                tempFile = self.download(movieName=self.parser.links[0], search=False, realMovieName=movieName)
                return tempFile
                
                
    def parseRating(self, tempFile, realMovieName):
        """
        Parse rating of the movie
        @param tempFile: temporary file to read from
        @param realMovieName: real movie name that is in the movieDB
        """
        self.ratingParser.rating = []
        tempFileStream = open(tempFile.name, "r")
        lines = tempFileStream.readlines()
        for line in lines:
            if re.match(r'.*h2.*', line):
                self.ratingParser.feed(line)
        tempFileStream.close()
        os.remove(tempFile.name)
        try:
            self.movieDB.findMovieByName(realMovieName).setRating(self.ratingParser.rating[0])
            print realMovieName
            print self.ratingParser.rating[0]
        except IndexError,e:
            pass        
        
    def parseTranslation(self, tempFile): 
        tempFileStream = open(tempFile.name, "r")
        lines = tempFileStream.readlines()
        for line in lines:
            if ((re.match(r'.*img.*', line, re.IGNORECASE)) or (re.match(r'.*h3.*', line, re.IGNORECASE))):
                self.translationParser.feed(line)
        tempFileStream.close()
        os.remove(tempFile.name)
        return self.translationParser.translation
        



        

        