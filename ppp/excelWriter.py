'''
Created on Mar 19, 2012

@author: Pavel Kostelnik
'''
import xlwt
import os
class ExcelWriter(object):
    '''
    xlwt module CAN NOT append to excel file -> have to create a new file with rating, then copy it back to data
    '''
    def writeMovieDB(self, db):
        wbk = xlwt.Workbook()
        mySheet = wbk.add_sheet("hodnoceni", cell_overwrite_ok=True)
        mySheet.write(0,0, "Nazev filmu")
        mySheet.write(0,1, "Prumerne hodnoceni")
        for i in range(0, len(db)):
            name = unicode(db[i].name)
            rating = unicode(db[i].rating)
            if rating=="0":
                rating = "Nenalezeno"
            mySheet.write(1+i,0, name)
            mySheet.write(1+i,1, rating)
        wbk.save(".."+os.sep+"data"+os.sep+"rating.xls")    
        
            
        



    def __init__(self):
        '''
        Constructor
        '''
        pass
        
        
        