# -*- coding: utf-8 -*-
'''
Created on Mar 7, 2012

@author: Radoslav Pesau
'''
from ppp.investigator import Investigator
import re,os
import difflib
import unicodedata
from HTMLParser import HTMLParser

class FDBInvestigator(Investigator):
    '''
    This class uses the CSFD as its main DB 
    '''
    skip = False
    class MyHTMLParser(HTMLParser):
        """
        Html parser to find movie in search results
        """
        recordData = False
        def handle_starttag(self, tag, attrs):
            if len(attrs) > 1:
                    if tag == "a":
                        for attr in attrs :
                             if attr[0]=="href": 
                                 if re.match(r'^./film/*',attr[1]):
                                     self.recordData=True   
                                     self.links.append(attr[1])
        def handle_data(self, data):
            if self.recordData:
                    self.data.append(data)
                    self.recordData=False     
        
    class MyRatingParser(HTMLParser):
        """
        Parser class to find rating in movie page
        """
        recordData = False
        def handle_starttag(self, tag, attrs):
            if tag == "div":
                if attrs:
                    if attrs[0][0] == "class":
                        if attrs[0][1] == "hodnoceni":
                            self.recordData = True
                
        def handle_endtag(self, tag):
            if self.recordData == True:
                if tag == "div":
                    self.recordData = False
                
        def handle_data(self, data):
            if self.recordData:
                if re.match(r"\d+.*", data):
                    data = re.sub("%", "", data)
                    self.rating.append(data)
    def __init__(self, app,  baseURL="http://www.fdb.cz"):
        '''
        Constructor
        '''
        super(FDBInvestigator, self).__init__(baseURL, app)
        self.parser = self.MyHTMLParser()
        self.ratingParser = self.MyRatingParser()
        

    def prepareQuery(self, movieName, search=True):
        if search:
            url = self.baseURL
            url += "/vyhledavani.html?&hledat="
            imovieName = unicodedata.normalize('NFD', unicode(movieName)).encode('ascii', 'ignore')
            movieQuery = re.sub(" ", "+", imovieName)
            url += movieQuery
            return url
        else:
            url = self.baseURL+movieName
            return url
 
        

    
    def parse(self, tempFile, movieName):
        """
        Parse HTML and download the particular page
        """
        self.parser.data = []
        self.parser.links = []
        tempFileStream = open(tempFile.name, "r")
        lines = tempFileStream.readlines()
        tempFileStream.close()
        for line in lines:
                if '<map id="mmap1" name="map1">' in line:
                    break;
                if '<div class="r5px">&nbsp;</div>' in line:
                    break;
                line = line.replace('"  "','"')
                if not "show_nahled('id_nahled_obecny', 0)" in line:
                    if not '<a class="external"' in line:
                        if not "<area" in line:
                            self.parser.feed(line)
            
        os.remove(tempFile.name)
        if self.parser.data:
            i=-1
            for x in self.parser.data:
                i=i+1
                if difflib.SequenceMatcher(None, x, movieName).ratio() > 0.1:
                    tempFile = self.download(movieName=self.parser.links[i], search=False, realMovieName=movieName)
                    return tempFile
                
                
    def parseRating(self, tempFile, realMovieName):
        """
        Parse rating of the movie
        @param tempFile: temporary file to read from
        @param realMovieName: real movie name that is in the movieDB
        """
        self.ratingParser.rating = []
        tempFileStream = open(tempFile.name, "r")
        lines = tempFileStream.readlines()
       
        for line in lines:
            if '<map id="mmap1" name="map1">' in line:
                break;
            if not '<a class="external"' in line:
                if not '<div class="img">' in line:
                    if not '<li class="lista_android">' in line:
                        if not '<!-- charset -->' in line:
                            if not '<p><a href="./../tv/1-ct1-ct2-nova-prima-family.html' in line:
                                self.ratingParser.feed(line)
            if '<div class="hodnoceni">' in line:
                break;
        tempFileStream.close()
        os.remove(tempFile.name)
        try:
            self.movieDB.findMovieByName(realMovieName).setRating(self.ratingParser.rating[0])
            print realMovieName
            print self.ratingParser.rating[0]
        except IndexError,e:
            pass        

        



        

        