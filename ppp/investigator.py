'''
Created on Mar 7, 2012

@author: Pavel Kostelnik
'''
import random, os
import string
import urllib2
from ppp.movieDB import MovieDB
#TODO this is to extend thread
class Investigator(object):
    '''
    This class is a pattern for all other rating investigators. 
    '''
    baseURL = None
    
    def prepareQuery(self, movieName, search=True):
        """
        Inheritance method to prepare all queries to request
        @param movieName: the string to change and parse for query preparation
        @param search: determine if the query is a search query or just rating download (I know my URL)
        """
        pass
    def random_string(self,length):
        """
        function to get random string
        """
        pool = string.letters + string.digits
        return ''.join(random.choice(pool) for i in xrange(length))
        
    def parseRating(self, tempFile, realMovieName):
        """
        Function to parse HTML to search for the movie rating
        @param tempFile: filestream in which the HTML is found
        @param realMovieName: what movie does the rating belong to (useful for writing the rating to movie db)
        """
        pass
    
    def download(self, movieName, search=True, realMovieName=None):
        """
        Function to download any URL 
        @param movieName: What movie name to search for
        @param search: whether the download is a search or plain URL rating download (I know my URL directly)
        @param realMovieName: Used only when downloading rating - movie name parameter holds direct URL, movieName is passed via realMovieName
        """
        
        query = self.prepareQuery(movieName, search=search)
        request = urllib2.Request(query)
        result = urllib2.urlopen(request)
        hashName = self.random_string(35)
        tempFile = open(".."+os.sep+"data"+os.sep+"temp"+os.sep+hashName+".html", "w+")
        file_size_dl = 0
        blocksize = 8192
        while True:
            mybuffer = result.read(blocksize)
            if not mybuffer:
                break
            file_size_dl += len(mybuffer)
            tempFile.write(mybuffer)
        tempFile.close()
        return tempFile
        
    def parse(self, tempFile, movieName):
        """
        The parser parses the HTML in temporary file and calls the download of the rating (download(search=False))
        @param tempFile: fileStream that has the HTML to parse in
        @param movieName: name of the movie to search for 
        
        """
        pass
        
    def run(self):
        self.movieDB = MovieDB() 
        self.movieDB.db = list(self.app.movieDB.db)
        for movie in self.movieDB.db:
            tempFile = self.download(movie.name)
            ratingFile = self.parse(tempFile, movie.name)
            if ratingFile:
                self.parseRating(ratingFile, movie.name)
    
    def __init__(self, mainURL,app):
        '''
        Constructor
        '''
        self.baseURL = mainURL
        self.app = app